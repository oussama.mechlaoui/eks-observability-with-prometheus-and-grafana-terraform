resource "aws_iam_role_policy" "alb_controller_policy" {
  name        = "${var.cluster_name}-AWSLoadBalancerControllerIAMPolicy"
  role = aws_iam_role.alb_controller_role.id
  policy = "${file("policies/alb-policy.json")}"
}

resource "aws_iam_role" "alb_controller_role" {
  name = "${var.cluster_name}-AWSLoadBalancerControllerIAMRole"
  
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Federated = module.eks.oidc_provider_arn
        }
        Condition = {
          StringEquals = {
              "${module.eks.oidc_provider}:sub" = "system:serviceaccount:kube-system:aws-load-balancer-controller"
          }    
        
        }
      },
    ]
  })
}

resource "helm_release" "alb_controller_helm" {
  #count=0
  depends_on=[module.eks,helm_release.kube_prometheus_stack]
  name       = "aws-load-balancer-controller"

  repository = "https://aws.github.io/eks-charts"
  chart      = "aws-load-balancer-controller"
  
  values = [<<EOF
clusterName: ${var.cluster_name}
region: ${var.region}
vpcId: ${module.vpc.vpc_id}
serviceAccount:
  annotations:
    eks.amazonaws.com/role-arn: ${aws_iam_role.alb_controller_role.arn}
EOF
  ]
  
  namespace = "kube-system"

}