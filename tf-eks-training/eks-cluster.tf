# EKS terraform module

data "aws_caller_identity" "current" {}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = var.cluster_name
  cluster_version = "1.22"
  subnet_ids         = slice(module.vpc.private_subnets, 0, 3)

  tags = {
    Environment = "training"
    auto-delete = "no"
  }

  vpc_id = module.vpc.vpc_id
  
  # aws-auth configmap
  create_aws_auth_configmap = true 
  manage_aws_auth_configmap = true
  
  #create oidc provider
  enable_irsa = true
  
  aws_auth_node_iam_role_arns_non_windows = [
    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${var.admin_user_name}-node-group-role",
  ]
  
  aws_auth_users = [
      {
        userarn = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/${var.admin_user_name}"
        username = "admin-user"
        groups = ["system:masters"]
      }
    ]

}

#VPC CNI

resource "aws_eks_addon" "vpc_cni" {
  cluster_name      = var.cluster_name
  addon_name        = "vpc-cni"
  addon_version     = var.vpc_cni_addon_version
  resolve_conflicts = "OVERWRITE"
  depends_on = [module.eks]
}

# NodeGroup IAM Role

resource "aws_iam_role" "ng_iam_role" {
  name = "${var.admin_user_name}-node-group-role"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

resource "aws_iam_role_policy_attachment" "ng_iam_role-AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.ng_iam_role.name
}

resource "aws_iam_role_policy_attachment" "ng_iam_role-AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.ng_iam_role.name
}

resource "aws_iam_role_policy_attachment" "ng_iam_role-AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.ng_iam_role.name
}

# NodeGroup worker nodegroup

resource "aws_eks_node_group" "terraform-ng" {
#  count=
  cluster_name    = var.cluster_name
  node_group_name = "${var.admin_user_name}-managed-nodgroup"
  node_role_arn   = aws_iam_role.ng_iam_role.arn
  subnet_ids      = [module.vpc.private_subnets[0],module.vpc.private_subnets[1],module.vpc.private_subnets[2]]
  
  
  remote_access {
    ec2_ssh_key = var.keypair
  }

  scaling_config {
    desired_size = 3
    max_size     = 3
    min_size     = 3
  }

  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_eks_addon.vpc_cni,
    aws_iam_role_policy_attachment.ng_iam_role-AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.ng_iam_role-AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.ng_iam_role-AmazonEC2ContainerRegistryReadOnly,
  ]
}

resource "aws_cloud9_environment_ec2" "cloud9_env" {
  #count=0
  instance_type = "m5.large"
  name          = "eks-env-${var.cluster_name}"
  subnet_id = module.vpc.public_subnets[0]
  owner_arn = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:user/${var.admin_user_name}"
}

resource "aws_security_group_rule" "cluster_c9_sg" {
  type              = "ingress"
  from_port         = 0
  to_port           = 65535
  protocol          = "-1"
  cidr_blocks = [module.vpc.vpc_cidr_block,module.vpc.vpc_secondary_cidr_blocks[0]]
  security_group_id = module.eks.cluster_security_group_id
  depends_on = [aws_cloud9_environment_ec2.cloud9_env]
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}
