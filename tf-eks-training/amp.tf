resource "aws_prometheus_workspace" "amp" {
  alias = "amp-kube-prometheus"
}

resource "aws_prometheus_rule_group_namespace" "alert_rule" {
  name         = "rules"
  workspace_id = aws_prometheus_workspace.amp.id
  data         = <<EOF
groups:
- name: SQS alerts
  rules:
  - alert: ReceivedMsgInDLQ
    expr: |
      sum(increase(aws_sqs_number_of_messages_sent_sum{queue_name=~".+-dlq"}[5m])) > 0
    for: 0m
    labels:
      severity: warning
    annotations:
      summary: Message received in DLQ (dlq {{ $labels.queue_name }}
      description: "Message received in DLQ (dlq {{ $labels.queue_name }}"
- name: Pods restart
  rules: 
  - alert: Container restarted 
    annotations: 
      summary: Container named {{$labels.container}} create by pod {{$labels.pod}} in {{$labels.namespace}} namespace was restarted 
    expr: | 
      sum(increase(kube_pod_container_status_restarts_total{namespace=~"(default|kube-prometheus)"}[2m])) by (pod,namespace,container) > 0 
    for: 0m 
    labels: 
      project: essilor
EOF
}

resource "aws_prometheus_alert_manager_definition" "alert_manager_configuration" {
  workspace_id = aws_prometheus_workspace.amp.id
  definition   = <<EOF
alertmanager_config: |
  route:
    receiver: default
  receivers:
    - name: default     
      sns_configs:
        - sigv4:
            region: ${var.region}
          topic_arn: ${aws_sns_topic.sns_topic_alarm.arn}
EOF
}