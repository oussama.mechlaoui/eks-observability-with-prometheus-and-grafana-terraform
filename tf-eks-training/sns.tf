resource "aws_sns_topic" "sns_topic_alarm" {
  name = "sns-alert-prometheus"
  policy = <<EOF
{
  "Version": "2008-10-17",
  "Id": "sns_policy_prometheus",
  "Statement": [
    {
      "Sid": "Allow_Publish_Alarms",
      "Effect": "Allow",
      "Principal": {
        "Service": "aps.amazonaws.com"
      },
      "Action": [
        "sns:Publish",
        "sns:GetTopicAttributes"
      ],
      "Resource": "arn:aws:sns:${var.region}:${data.aws_caller_identity.current.account_id}:sns-alert-prometheus",
      "Condition": {
        "ArnEquals": {
            "aws:SourceArn": "${aws_prometheus_workspace.amp.arn}"
        },
        "StringEquals": {
            "AWS:SourceAccount": "${data.aws_caller_identity.current.account_id}"
        }
      }
    }
  ]
}
EOF
} 

resource "aws_sns_topic_subscription" "sns_subscription_alarm" {
  topic_arn = "${aws_sns_topic.sns_topic_alarm.arn}"
  protocol  = var.sns_protocol
  endpoint  = var.sns_endpoint
}

resource "aws_sqs_queue" "demo_queue" {
  name                      = "demo-queue-dlq"
  delay_seconds             = 90
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
}