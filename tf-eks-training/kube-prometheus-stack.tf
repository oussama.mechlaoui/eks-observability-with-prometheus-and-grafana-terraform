locals {
  sa_prometheus = "prometheus-server-sa"
  sa_grafana = "grafana-server-sa"
}

resource "aws_iam_role_policy" "prometheus_policy" {
  name = "${var.cluster_name}-AMPIngestPolicy"
  role = aws_iam_role.prometheus_role.id
  policy = "${file("policies/prometheus-policy.json")}"
}

resource "aws_iam_role" "prometheus_role" {
  name = "${var.cluster_name}-amp-irsa-training-role"
  
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Federated = module.eks.oidc_provider_arn
        }
        Condition = {
          StringEquals = {
              "${module.eks.oidc_provider}:sub" = "system:serviceaccount:${var.monitoring_namespace}:${local.sa_prometheus}"
          }    
        
        }
      }
    ]
  })
}

resource "aws_iam_role_policy" "grafana_policy" {
  name = "${var.cluster_name}-AMPQueryPolicy"
  role = aws_iam_role.grafana_role.id
  policy = "${file("policies/grafana-policy.json")}"
}

resource "aws_iam_role" "grafana_role" {
  name = "${var.cluster_name}-grafana-irsa-training-role"
  
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Federated = module.eks.oidc_provider_arn
        }
        Condition = {
          StringEquals = {
              "${module.eks.oidc_provider}:sub" = "system:serviceaccount:${var.monitoring_namespace}:${local.sa_grafana}"
          }    
        
        }
      }
    ]
  })
}

resource "kubernetes_namespace" "kube_prometheus" {
  metadata {
    name = var.monitoring_namespace
  }
}

resource "helm_release" "kube_prometheus_stack" {
  #count=0
  depends_on=[module.eks,aws_prometheus_workspace.amp,kubernetes_namespace.kube_prometheus,aws_acm_certificate.acm_grafana_cert]
  name       = "kube-prometheus-stack"

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "kube-prometheus-stack"
  
  values = [
    templatefile("${path.root}/templates/kube-prom-values.yaml.tpl",
    {
      url: aws_prometheus_workspace.amp.prometheus_endpoint
      region: var.region
      grafana_endpoint: var.grafana_endpoint
      acm_cert: aws_acm_certificate.acm_grafana_cert.arn
      prometheus_iam: aws_iam_role.prometheus_role.arn
      grafana_iam: aws_iam_role.grafana_role.arn
    }
    )
  ]
  
  namespace = var.monitoring_namespace

}

resource "aws_acm_certificate" "acm_grafana_cert" {
  domain_name       = var.grafana_endpoint
  validation_method = "DNS"
}

data "aws_route53_zone" "r53_validation" {
  name         = var.acm_grafana_domain
  private_zone = false
}

resource "aws_route53_record" "rs_record" {
  for_each = {
    for dvo in aws_acm_certificate.acm_grafana_cert.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.aws_route53_zone.r53_validation.zone_id
}

resource "aws_acm_certificate_validation" "example" {
  certificate_arn         = aws_acm_certificate.acm_grafana_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.rs_record : record.fqdn]
}

data "kubernetes_ingress_v1" "grafana_ingress" {
  depends_on = [module.eks,helm_release.kube_prometheus_stack,helm_release.alb_controller_helm]
  #count = 0
  metadata {
    name = "kube-prometheus-stack-grafana"
    namespace = var.monitoring_namespace
  }
}

data "aws_elb_hosted_zone_id" "main" {}

resource "aws_route53_record" "www" {
  depends_on = [data.kubernetes_ingress_v1.grafana_ingress,helm_release.kube_prometheus_stack]
  #count= try(data.kubernetes_ingress_v1.grafana_ingress.metadata.0.uid, "") != "" ? 1 : 0
  #count = data.kubernetes_ingress_v1.grafana_ingress != null ? 1 : 0
  #count=0
  count= var.on_delete ? 0 : 1
  zone_id = data.aws_route53_zone.r53_validation.zone_id
  name    = var.grafana_endpoint
  type    = "A"

  alias {
    name                   = data.kubernetes_ingress_v1.grafana_ingress.status.0.load_balancer.0.ingress.0.hostname
    zone_id                = data.aws_elb_hosted_zone_id.main.id
    evaluate_target_health = true
  }
}
