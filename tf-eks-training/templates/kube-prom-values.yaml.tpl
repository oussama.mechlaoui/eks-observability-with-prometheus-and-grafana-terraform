additionalPrometheusRules:
- name: sqs-alert-rule
  groups:
    - name: my_group
      rules:
      - alert: ReceivedMsgInDLQ
        expr: max_over_time(aws_sqs_number_of_messages_sent_sum{queue_name=~".+-dlq"}[5m]) > 0
        for: 0m
        labels:
          severity: warning
        annotations:
          summary: Message received in DLQ (dlq {{ $labels.queue_name }}
          description: "Message received in DLQ (dlq {{ $labels.queue_name }}"
    - name: custom-node-exporter-rules
      rules:
      - alert: Container restarted 
        annotations: 
          summary: Container named {{$labels.container}} create by pod {{$labels.pod}} in {{$labels.namespace}} namespace was restarted 
        expr: | 
          sum(increase(kube_pod_container_status_restarts_total{namespace=~"(default|kube-prometheus)"}[2m])) by (pod,namespace,container) > 0 
        for: 0m 
        labels: 
          project: essilor

alertmanager:
  enabled: false
  
grafana:
  enabled: enable
  adminPassword: admin
  serviceAccount:
    name: grafana-server-sa
    annotations: 
      eks.amazonaws.com/role-arn: ${grafana_iam}
  grafana.ini:
    auth:
      sigv4_auth_enabled: true
  additionalDataSources:
    - name: Amazon Managed Prometheus
      type: prometheus
      access: proxy
      url: ${url}
      isDefault: false
      jsonData:
        timeInterval: 15s
        sigV4Auth: true
        sigV4Region: ${region}
        sigV4AuthType: default
  sidecar:
    datasources:
      defaultDatasourceEnabled: true
  ingress:
    enabled: true
    annotations:
      alb.ingress.kubernetes.io/certificate-arn: ${acm_cert}
      alb.ingress.kubernetes.io/listen-ports: '[{"HTTPS":443}]'
      alb.ingress.kubernetes.io/success-codes: 200-302
      alb.ingress.kubernetes.io/scheme: internet-facing
      alb.ingress.kubernetes.io/target-type: ip
      kubernetes.io/ingress.class: alb
  
    hosts:
    - ${grafana_endpoint}
    path: /*
    pathType: ImplementationSpecific

kubeEtcd:
  enabled: false
 
kubeScheduler:
  enabled: false

kubeControllerManager:
  enabled: false

nodeExporter:
  enabled: true

prometheus:
  serviceAccount:
    name: prometheus-server-sa
    annotations: 
      eks.amazonaws.com/role-arn: ${prometheus_iam}
  prometheusSpec:
    serviceMonitorSelectorNilUsesHelmValues: false
#    resources: 
#      limits:
#        cpu: "2048m"
    remoteWrite:
    - url: ${url}api/v1/remote_write
      sigv4:
        region: ${region}