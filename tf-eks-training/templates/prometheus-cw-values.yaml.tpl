config: |-
  # This is the default configuration for prometheus-cloudwatch-exporter
  region: ${region}
  period_seconds: 30
  delay_seconds: 180
  metrics:
  - aws_namespace: AWS/SQS
    aws_metric_name: NumberOfMessagesSent
    aws_dimensions: [QueueName]
    aws_statistics: [Sum]
    
serviceAccount:
  name: cloudwatch-exporter-sa
  annotations:
    eks.amazonaws.com/role-arn: ${cw_exporter_iam}

serviceMonitor:
  enabled: true
  namespace: ${namespace}
  interval: 15s
  telemetryPath: /metrics