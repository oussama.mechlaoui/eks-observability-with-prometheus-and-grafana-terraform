variable "admin_user_name" {
  type = string
  description = "Enter your aws user"
  
}

variable "keypair" {
  type = string
  description = "Enter your keypair used for SSHing worker nodes"
}

variable "vpc_cni_addon_version" {
  type        = string
  default     = "v1.11.0-eksbuild.1"
}

variable "region" {
  default     = "eu-west-1"
  description = "AWS region"
}

variable "cluster_name" {
  default     = "eu-west-1"
  description = "AWS region"
}

variable "vpc_name" {
  default     = "eu-west-1"
  description = "AWS region"
}

variable "monitoring_namespace" {
  type = string
  default = "kube-prometheus"
}

variable "grafana_endpoint" {
  type = string
}


variable "acm_grafana_domain" {
  type = string
}

variable "sns_protocol" {
  type = string
}

variable "sns_endpoint" {
  type = string
}

variable "on_delete" {
  type = bool
  default = false
}