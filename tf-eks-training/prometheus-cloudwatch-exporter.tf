resource "aws_iam_role_policy" "cloudwatch_exporter_policy" {
  name = "${var.cluster_name}-CloudWatchExporterPolicy"
  role = aws_iam_role.cloudwatch_exporter_role.id
  policy = "${file("policies/cloudwatch-exporter-policy.json")}"
}

resource "aws_iam_role" "cloudwatch_exporter_role" {
  name = "${var.cluster_name}-cloudwatch-exporter-irsa-training-role"
  
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRoleWithWebIdentity"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Federated = module.eks.oidc_provider_arn
        }
        Condition = {
          StringEquals = {
              "${module.eks.oidc_provider}:sub" = "system:serviceaccount:${var.monitoring_namespace}:cloudwatch-exporter-sa"
          }    
        
        }
      }
    ]
  })
}


resource "helm_release" "prometheus-cloudwatch-exporter" {
  depends_on=[module.eks,aws_prometheus_workspace.amp,helm_release.kube_prometheus_stack]
  name       = "prometheus-cloudwatch-exporter"

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus-cloudwatch-exporter"
  
  values = [
    templatefile("${path.root}/templates/prometheus-cw-values.yaml.tpl",
    {
        region: var.region
        cw_exporter_iam: aws_iam_role.cloudwatch_exporter_role.arn
        namespace: var.monitoring_namespace
    }
    )
  ]
  
  namespace = var.monitoring_namespace

}