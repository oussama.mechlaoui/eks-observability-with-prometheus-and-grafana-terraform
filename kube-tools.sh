#!/bin/bash

set -xe

CLUSTER_NAME=$2
REGION=$1
ACCOUNT_ID=$(aws sts get-caller-identity --query Account --output text)

#install aws-iam-authenticator
curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.21.2/2021-07-05/bin/linux/amd64/aws-iam-authenticator
chmod +x ./aws-iam-authenticator
mkdir -p $HOME/bin && cp ./aws-iam-authenticator $HOME/bin/aws-iam-authenticator && export PATH=$PATH:$HOME/bin
echo 'export PATH=$PATH:$HOME/bin' >> ~/.bashrc

#install awscli v2

curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install --update -i /usr/local/aws-cli -b /usr/local/bin

#install kubectl with bash completion
curl -o kubectl https://amazon-eks.s3.us-west-2.amazonaws.com/1.21.2/2021-07-05/bin/linux/amd64/kubectl
chmod +x ./kubectl
mkdir -p $HOME/bin && cp ./kubectl $HOME/bin/kubectl && export PATH=$PATH:$HOME/bin
echo 'export PATH=$PATH:$HOME/bin' >> ~/.bashrc

sudo yum -y install jq gettext bash-completion moreutils

echo 'source <(kubectl completion bash)' >>~/.bashrc
source ~/.bashrc

#install eksctl
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin

#prepare kubeconfig file

payload=$(aws eks describe-cluster --name $CLUSTER_NAME --query 'cluster.{CA: certificateAuthority.data, Endpoint: endpoint}')


CA=$(echo $payload | jq -rc .CA)
ENDPOINT=$(echo $payload | jq -rc .Endpoint)

if [ ! -d "/home/ec2-user/.kube" ] 
then
    mkdir /home/ec2-user/.kube
fi

cat <<EOF > ~/.kube/config
apiVersion: v1
clusters:
- cluster:
    server: $ENDPOINT
    certificate-authority-data: $CA
  name: handson-eks
contexts:
- context:
    cluster: handson-eks
    user: handson-user
  name: handson-eks-context
current-context: handson-eks-context
kind: Config
preferences: {}
users:
- name: handson-user
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1beta1
      command: aws
      args:
        - --region
        - $REGION
        - eks
        - get-token
        - --cluster-name
        - $CLUSTER_NAME
EOF

#install helm - package manager for kubernetes

curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 > get_helm.sh
chmod 700 get_helm.sh
./get_helm.sh

/home/ec2-user/bin/kubectl apply -k "github.com/aws/eks-charts/stable/aws-load-balancer-controller/crds?ref=master"
helm repo add eks https://aws.github.io/eks-charts
helm repo update